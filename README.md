# Install

Click "Install Helbreath Timer" (+ Icon in Chrome address bar) to launch the app in a standalone minimal window.

# Setttings

## HP

Time when HP Regens start. HP Regens every 15s.

## MP

Time when MP Regens start. MP Regens every 20s.

## Login

Both HP and MP timers start at the time you log in. Set both values to Login time. Relog to reset,

### Hunger

Hunger can cause HP/MP times to desync. This is why there is an input for both HP and MP adjustments.

## Offset

This app uses your computer's local time which does not match server time. You will have to adjust this value to get in sync with server time.

## Blink

Background will blink red/blue 2 seconds before regen.

## Sound

Text-To-Speech voice will say "HP"/"MP"/"Both" 2 seconds before regen.
